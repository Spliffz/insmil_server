"Advertisements"
{
	"1"
	{
		"type"		"S"
		"text"		"{LIGHTGREEN}RULES: No TeamKilling! No Cheating! No Voice/Chat Spamming! No Whining! No Racism! Only English in voip/chat! FOLLOW ADMIN INSTRUCTIONS!"
	}
	"2"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: 1.Kill All OpFor. 2.Place Explosives. 3.Regroup. 4. Take OBJs With Team. 5.Repel Counter Attack. 6. Resupply."
	}
	"3"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: Molotovs, Smoke and Incendiary grenades are NOT needed."
	}
	"4"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: FIRING LINES: Do NOT step in front of weapons."
	}
	"5"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: ON POINT: CROUCH so we can shoot over your head."
	}
	"6"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: FIRING LINES: Do NOT run in front of teammates guns. If you do you will be shot."
	}
	"7"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: Slow is Smooth, Smooth is Fast!"
	}
	"8"
	{
		"type"		"S"
		"text"		"{GREEN}SOP: This crew comes with Rules and Expectations. The Rules are there to Insure our Victory. The Expectation is those wanting to be a part of this crew follow the Rules."
	}
	"9"
	{
		"type"		"S"
		"text"		"Check out {LIGHTGREEN}www.insmilitia.com{DEFAULT} for more information!"
	}
	"10"
	{
		"type" 		"S"
		"text"		"Lasers give away your position to the enemy! They will shoot rockets at you and kill you and your surrounding teammates! Turn them off when you do not need them."
	}
	"11"
	{
		"type"		"S"
		"text"		"A Big Thanks to the NWI Dev Team!"
	}
}